from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.
@login_required
def project_list(request):
    project_instace = Project.objects.filter(owner=request.user.id)
    context = {
        "project_instance": project_instace,
    }
    return render(request, "projects/list.html", context)


@login_required
def project_details(request, id):
    project_task = get_object_or_404(Project, id=id)
    context = {
        "project_task": project_task,
    }
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
