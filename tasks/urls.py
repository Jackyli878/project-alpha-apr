from django.urls import path
from tasks.views import create_task, task_list
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path(
        "create/",
        login_required(create_task),
        name="create_task",
    ),
    path("mine/", login_required(task_list), name="show_my_tasks"),
]
